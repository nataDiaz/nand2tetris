
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;

/**
 * @author Natalia Diaz
 */
public class main {

    static private Hashtable<String, Integer> symbolTable = new Hashtable<String, Integer>();
    static private int nextDataAddress = 16;
    static private int nextProgramAddress = 0;
    private static final List<String> results = new ArrayList<String>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter file name:");
        String fileName = sc.next();

        File file = new File(fileName);

        try {
            fillSymbolTable();
            checkLabels(file);
            translate(file);

            // Write hack file 
            File fileOut = new File(fileName.replace(".asm", ".hack"));
            FileOutputStream fos = new FileOutputStream(fileOut);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            for (String line : results) {
                bw.write(line);
                bw.newLine();
            }

            bw.close();
        } catch (FileNotFoundException e) {
            // File not found
            e.printStackTrace();
        } catch (IOException e) {
            // Error when reading the file
            e.printStackTrace();
        }
    }

    public static String getComp(String txt) {
        if (txt.contains("=")) {
            txt = txt.replace(txt.substring(0, txt.indexOf("=") + 1), "");
        }

        if (txt.contains(";")) {
            txt = txt.replace(txt.substring(txt.indexOf(";")), "");
        }

        switch (txt) {
            case "0":
                return "0101010";
            case "1":
                return "0111111";
            case "-1":
                return "0111010";
            case "D":
                return "0001100";
            case "A":
                return "0110000";
            case "!D":
                return "0001101";
            case "!A":
                return "0110001";
            case "-D":
                return "0001111";
            case "-A":
                return "0110011";
            case "D+1":
                return "0011111";
            case "A+1":
                return "0110111";
            case "D-1":
                return "0001110";
            case "A-1":
                return "0110010";
            case "D+A":
                return "0000010";
            case "D-A":
                return "0010011";
            case "A-D":
                return "0000111";
            case "D&A":
                return "0000000";
            case "D|A":
                return "0010101";
            case "M":
                return "1110000";
            case "!M":
                return "1110001";
            case "-M":
                return "1110011";
            case "M+1":
                return "1110111";
            case "M-1":
                return "1110010";
            case "D+M":
                return "1000010";
            case "D-M":
                return "1010011";
            case "M-D":
                return "1000111";
            case "D&M":
                return "1000000";
            default:
                return "1010101";
        }
    }

    public static String getDest(String txt) {
        if (txt.contains("=")) {
            txt = txt.substring(0, txt.indexOf("="));

            switch (txt) {
                case "M":
                    return "001";
                case "D":
                    return "010";
                case "MD":
                    return "011";
                case "A":
                    return "100";
                case "AM":
                    return "101";
                case "AD":
                    return "110";
                default:
                    return "111";
            }
        }

        return "000";
    }

    public static String getJump(String txt) {
        if (txt.contains(";")) {
            txt = txt.substring(txt.indexOf(";") + 1);

            switch (txt) {
                case "JGT":
                    return "001";
                case "JEQ":
                    return "010";
                case "JGE":
                    return "011";
                case "JLT":
                    return "100";
                case "JNE":
                    return "101";
                case "JLE":
                    return "110";
                default:
                    return "111";
            }
        }

        return "000";
    }

    public static void aInstruction(String txt) {
        txt = txt.replace(Character.toString(txt.charAt(0)), "");
        String result;

        if (symbolTable.containsKey(txt)) {
            result = formatNumberAs16bBinary(symbolTable.get(txt));
        } else if (txt.matches("-?\\d+(\\.\\d+)?")) {
            result = formatNumberAs16bBinary(Integer.parseInt(txt));
        } else {
            symbolTable.put(txt, nextDataAddress);
            result = formatNumberAs16bBinary(nextDataAddress);
            nextDataAddress++;
        }

        results.add(result);
    }

    public static void cInstruction(String txt) {
        StringBuilder result = new StringBuilder();
        result.append("111");
        result.append(getComp(txt));
        result.append(getDest(txt));
        result.append(getJump(txt));

        results.add(result.toString());
    }

    public static void translate(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String line;

        while ((line = br.readLine()) != null) {
            line = line.trim();
            line = cleanLine(line);

            if (!line.equals("") && !Character.toString(line.charAt(0)).equals("(")) {
                if (Character.toString(line.charAt(0)).equals("@")) {
                    aInstruction(line);
                } else {
                    cInstruction(line);
                }
            }
        }

        br.close();
    }

    public static void checkLabels(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String line;

        while ((line = br.readLine()) != null) {
            line = line.trim();
            line = cleanLine(line);

            if (!line.equals("")) {
                if (Character.toString(line.charAt(0)).equals("(")) {
                    line = line.substring(1, line.length() - 1);

                    symbolTable.put(line, nextProgramAddress);
                } else {
                    nextProgramAddress++;
                }
            }
        }

        br.close();
    }

    public static void fillSymbolTable() {
        symbolTable.put("SP", 0);
        symbolTable.put("LCL", 1);
        symbolTable.put("ARG", 2);
        symbolTable.put("THIS", 3);
        symbolTable.put("THAT", 4);
        symbolTable.put("R0", 0);
        symbolTable.put("R1", 1);
        symbolTable.put("R2", 2);
        symbolTable.put("R3", 3);
        symbolTable.put("R4", 4);
        symbolTable.put("R5", 5);
        symbolTable.put("R6", 6);
        symbolTable.put("R7", 7);
        symbolTable.put("R8", 8);
        symbolTable.put("R9", 9);
        symbolTable.put("R10", 10);
        symbolTable.put("R11", 11);
        symbolTable.put("R12", 12);
        symbolTable.put("R13", 13);
        symbolTable.put("R14", 14);
        symbolTable.put("R15", 15);
        symbolTable.put("SCREEN", 16384);
        symbolTable.put("KBD", 24576);
    }

    public static String formatNumberAs16bBinary(int num) {
        String binaryNumber = Integer.toBinaryString(num);

        return String.format("%16s", binaryNumber).replace(' ', '0');
    }

    public static String cleanLine(String line) {
        int commentIndex = line.indexOf("/");

        if (commentIndex != -1) {
            line = line.replace(line.substring(commentIndex), "");
        }

        return line.trim();
    }
}
