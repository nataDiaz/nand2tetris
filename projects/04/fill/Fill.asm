// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed.
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Get SCREEN register
@SCREEN
D = A
@i // Set to the i variable the value in SCREEN - 1
M = D - 1

// Initialize POS variable in i
// POS will represent the current pixel to fill
D = M
@POS
M = D

// Get KBD register
@KBD
D = A
@j // Set to the j variable the value in KBD
M = D

// Start watching
(LOOP)
@KBD
D = M // Get KBD to see if it's 1 or 0
@FILL_WHITE
D;JEQ // FBD = 0 -> Fill White
@FILL_BLACK
0;JMP // FBD = 1 -> FIll Black

(FILL_WHITE)
@i // i moves from the last to the first pixel in the screen
D = M
@POS
D = D - M // If D - M = 0 -> No key has been pressed yet, so go back to the loop
@LOOP
D;JEQ

// If a key was pressed, decrease POS and set the corresponding pixel to white
// This happens meantime the loop reach all the screen size
@POS
M = M - 1 // Decrease POS -> POS--
A = M
M = 0 // Set pixel in RAM[RAM[POS]] to 0 -> White
@LOOP // Go back to the loop
0;JMP

(FILL_BLACK)
@j // j moves from the first to the last pixel in the screen
D = M
@POS
D = D - M // If D - M = 0 -> A key is being pressed, all the screen filled
@LOOP // Keep in the loop
D;JEQ

// If a key is being pressed and there are remaining pixels to fill
// Increase POS value to move on and fill all the pixels
@POS
A = M // A = RAM[POS]
// -1 = 1111111111111111 -> Black
M = -1 // RAM[RAM[POS]] = -1 -> Set the current value in register RAM[POS] to -1
@POS
M = M + 1 // Increase POS value -> POS++
@LOOP // Go back to the loop
0;JMP
