// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

@R0
D = M // Get value in RAM[0]
@n
M = D // Assign value in RAM[0] to variable n

@i
M = 1 // Initialize i = 1

@sum
M = 0 // Initialize sum = 0

// START LOOP: for (i=1; i=R1; i++)
(LOOP)
@i
D = M // D = i
@R1
D = D - M // D = i - R1
@END
D;JGT // if (i - R1) > 0 then END LOOP

@n
D = M // D = R0
@sum
D = D + M // D = R0 + sum
M = D     // sum += R0

@i
M = M + 1 // i++
@LOOP
0;JMP

// STOP LOOP
// Sum is the result of R0 * R1
(END)
@sum
D = M // D = sum
@R2
M = D // R2 = sum
